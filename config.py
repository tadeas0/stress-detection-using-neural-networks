from enum import Enum
import configparser
cp = configparser.ConfigParser()
cp.read('config.ini')

RESPIBAN_SAMPLE_RATE = 700
CLAS_SAMPLING_RATE = 256
TARGET_SAMPLE_RATE = 500
WINDOW_LEN = 30
RANDOM_STATE = 12345

CLAS_ORIGINAL_DIR = cp.get('DEFAULT', 'clas_dir')
WESAD_ORIGINAL_DIR = cp.get('DEFAULT', 'wesad_dir')

OUT_DIR = './out'
TRAIN_OUT_DIR = f'{OUT_DIR}/train_out'
EXAMPLE_DATA_DIR = './example_data'
CLAS_FORMATTED_DIR = f'{OUT_DIR}/clas_formatted'
WESAD_FORMATTED_DIR = f'{OUT_DIR}/wesad_formatted'

WESAD_LABEL_STRINGS = {
    0: 'Not defined',
    1: 'Baseline',
    2: 'Stress',
    3: 'Fun',
    4: 'Medi',
    5: 'Ignore',
    6: 'Ignore',
    7: 'Ignore',
    8: 'Counting exercise'
}

CLAS_LABEL_STRINGS = {
    'Baseline': 0,
    'Neutral': 1,
    'Video clip': 2,
    'Math Test': 3,
    'Math Test Response': 4,
    'Pictures': 5,
    'Stroop Test': 6,
    'Stroop Test Response': 7,
    'IQ Test': 8,
    'IQ Test Response': 9
}

NEW_DATASET_ROOT = './datasets/jan_hejda'

HRV_BOUNDS = {
    'heart_rate_calm': 72,
    'heart_rate_cog_load': 75,
    'mean_rr_calm': 0.81,
    'mean_rr_cog_load': 0.79,
    'rmssd_calm': 0.032,
    'rmssd_cog_load': 0.03
}


class WesadLabel(Enum):
    NOT_DEFINED = 0
    BASELINE = 1
    STRESS = 2
    FUN = 3
    MEDI = 4
    IGNORE_1 = 5
    IGNORE_2 = 6
    IGNORE_3 = 7
    COG_LOAD = 8


class ClasLabel(Enum):
    BASELINE = 0
    NEUTRAL = 1
    VIDEO_CLIP = 2
    MATH_TEST = 3
    MATH_TEST_RESPONSE = 4
    PICTURES = 5
    STROOP_TEST = 6
    STROOP_TEST_RESPONSE = 7
    IQ_TEST = 8
    IQ_TEST_RESPONSE = 9


WESAD_POSITIVE = [WesadLabel.COG_LOAD.value]
WESAD_NEGATIVE = [WesadLabel.BASELINE.value]

CLAS_POSITIVE = [
    ClasLabel.MATH_TEST.value, ClasLabel.STROOP_TEST.value,
    ClasLabel.IQ_TEST.value
]
CLAS_NEGATIVE = [ClasLabel.BASELINE.value, ClasLabel.NEUTRAL.value]
