from keras.layers import Input, Conv1D, MaxPooling1D, Flatten, Dense, concatenate, Dropout, GlobalAveragePooling1D
from keras.models import Model


def mh_model1(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=8, kernel_size=15, strides=2,
                       activation='relu')(inp)
        pool1 = MaxPooling1D(pool_size=4, strides=4)(conv1)
        conv2 = Conv1D(filters=16, kernel_size=7, strides=2,
                       activation='relu')(pool1)
        pool2 = MaxPooling1D(pool_size=4, strides=4)(conv2)
        conv3 = Conv1D(filters=32, kernel_size=3, strides=1,
                       activation='relu')(pool2)
        pool3 = MaxPooling1D(pool_size=2, strides=2)(conv3)
        flat = Flatten()(pool3)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model2(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=8,
                       kernel_size=100,
                       strides=2,
                       activation='relu')(inp)
        pool1 = MaxPooling1D(pool_size=4, strides=4)(conv1)
        conv2 = Conv1D(filters=16, kernel_size=6, strides=2,
                       activation='relu')(pool1)
        pool2 = MaxPooling1D(pool_size=4, strides=4)(conv2)
        conv3 = Conv1D(filters=32, kernel_size=3, activation='relu')(pool2)
        pool3 = MaxPooling1D(pool_size=2, strides=2)(conv3)
        drop = Dropout(0.2)(pool3)
        flat = Flatten()(drop)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model3(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=8, kernel_size=15, strides=2,
                       activation='relu')(inp)
        drop1 = Dropout(0.3)(conv1)
        pool1 = MaxPooling1D(pool_size=4, strides=4)(drop1)
        conv2 = Conv1D(filters=16, kernel_size=7, strides=2,
                       activation='relu')(pool1)
        drop2 = Dropout(0.3)(conv2)
        pool2 = MaxPooling1D(pool_size=4, strides=4)(drop2)
        conv3 = Conv1D(filters=32, kernel_size=3, strides=1,
                       activation='relu')(pool2)
        drop3 = Dropout(0.3)(conv3)
        pool3 = MaxPooling1D(pool_size=2, strides=2)(drop3)
        flat = Flatten()(pool3)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model4(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=8, kernel_size=15, activation='relu')(inp)
        drop1 = Dropout(0.3)(conv1)
        pool1 = MaxPooling1D(pool_size=4)(drop1)
        conv2 = Conv1D(filters=16, kernel_size=7, activation='relu')(pool1)
        drop2 = Dropout(0.3)(conv2)
        pool2 = MaxPooling1D(pool_size=4)(drop2)
        conv3 = Conv1D(filters=32, kernel_size=3, activation='relu')(pool2)
        drop3 = Dropout(0.3)(conv3)
        pool3 = MaxPooling1D(pool_size=2)(drop3)
        flat = Flatten()(pool3)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model5(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=32, kernel_size=3, activation='relu')(inp)
        drop1 = Dropout(0.3)(conv1)
        pool1 = MaxPooling1D(pool_size=4)(drop1)
        conv2 = Conv1D(filters=64, kernel_size=15, activation='relu')(pool1)
        pool2 = MaxPooling1D(pool_size=4)(conv2)
        flat = Flatten()(pool2)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model6(input_shapes):
    conv_blocks = []
    inputs = []
    for i in input_shapes:
        inp = Input(shape=i)
        conv1 = Conv1D(filters=32, kernel_size=3, activation='relu')(inp)
        pool1 = MaxPooling1D(pool_size=4)(conv1)
        flat = Flatten()(pool1)
        conv_blocks.append(flat)
        inputs.append(inp)

    if len(input_shapes) > 1:
        merged = concatenate(conv_blocks)
        dense1 = Dense(100, activation='relu')(merged)
    else:
        dense1 = Dense(100, activation='relu')(conv_blocks[0])
    output = Dense(1, activation='sigmoid')(dense1)

    return Model(inputs=inputs, outputs=output)


def mh_model7(input_shapes):
    inp = Input(shape=input_shapes[0])
    inputs = [inp]
    conv1 = Conv1D(128, 55, activation='relu')(inp)
    pool1 = MaxPooling1D(10)(conv1)
    drop1 = Dropout(0.5)(pool1)
    conv2 = Conv1D(128, 25, activation='relu')(drop1)
    pool2 = MaxPooling1D(5)(conv2)
    drop2 = Dropout(0.5)(pool2)
    conv3 = Conv1D(128, 5, activation='relu')(drop2)
    pool3 = GlobalAveragePooling1D()(conv3)
    flatten = Flatten()(pool3)

    dense1 = Dense(256, kernel_initializer='normal',
                   activation='relu')(flatten)
    drop3 = Dropout(0.5)(dense1)
    dense2 = Dense(128, kernel_initializer='normal', activation='relu')(drop3)
    drop4 = Dropout(0.5)(dense2)
    dense3 = Dense(64, kernel_initializer='normal', activation='relu')(drop4)
    drop5 = Dropout(0.5)(dense3)

    output = Dense(1, activation='sigmoid')(drop5)

    return Model(inputs=inputs, outputs=output)