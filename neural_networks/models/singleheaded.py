from keras.models import Sequential
from keras.layers import Conv1D, Dropout, MaxPooling1D, Flatten, Dense, GlobalAveragePooling1D


def sh_model1(input_shape):
    model = Sequential()
    model.add(
        Conv1D(filters=64,
               kernel_size=3,
               activation='relu',
               input_shape=input_shape))
    model.add(Conv1D(filters=64, kernel_size=3, activation='relu'))
    model.add(Dropout(0.5))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    return model


def sh_model2(input_shape):
    model = Sequential()
    model.add(
        Conv1D(filters=8,
               kernel_size=32,
               strides=2,
               activation='relu',
               input_shape=input_shape))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=16, kernel_size=7, strides=2, activation='relu'))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D(pool_size=2, strides=2))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    return model


def sh_model3(input_shape):
    model = Sequential()
    model.add(Conv1D(128, 5, activation='relu', input_shape=input_shape))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(GlobalAveragePooling1D())
    model.add(Dense(256, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    return model


def sh_model4(input_shape):

    model = Sequential()

    model.add(Conv1D(128, 55, input_shape=input_shape, activation='relu'))
    model.add(MaxPooling1D(10))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 25, activation='relu'))
    model.add(MaxPooling1D(5))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(GlobalAveragePooling1D())
    model.add(Flatten())

    model.add(Dense(256, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(1, activation='sigmoid'))

    return model


def sh_model5(input_shape):
    model = Sequential()
    model.add(
        Conv1D(filters=8,
               kernel_size=100,
               strides=2,
               activation='relu',
               input_shape=input_shape))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=16, kernel_size=6, strides=2, activation='relu'))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D(pool_size=2, strides=2))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    return model


def sh_model6(input_shape):
    model = Sequential()
    model.add(
        Conv1D(filters=8,
               kernel_size=15,
               strides=2,
               activation='relu',
               input_shape=input_shape))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=16, kernel_size=7, strides=2, activation='relu'))
    model.add(MaxPooling1D(pool_size=4, strides=4))
    model.add(Conv1D(filters=32, kernel_size=3, strides=1, activation='relu'))
    model.add(Dropout(0.5))
    model.add(MaxPooling1D(pool_size=2, strides=2))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    return model
