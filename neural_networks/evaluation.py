import numpy as np
from sklearn.metrics import f1_score, confusion_matrix
import csv


def get_evaluation_metrics(model, x_test, y_test):
    predictions = np.round(model.predict(x_test).flatten())

    tn = 0
    tp = 0
    fn = 0
    fp = 0

    cmtx = confusion_matrix(y_test, predictions)
    tn = cmtx[0][0]
    fp = cmtx[0][1]
    fn = cmtx[1][0]
    tp = cmtx[1][1]

    accuracy = (tn + tp) / (tn + tp + fn + fp)
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    precision = tp / (tp + fp)

    f1 = f1_score(y_test, predictions)

    return {
        'true_positives': tp,
        'true_negatives': tn,
        'false_positives': fp,
        'false_negatives': fn,
        'accuracy': accuracy,
        'sensitivity': sensitivity,
        'specificity': specificity,
        'precision': precision,
        'f1': f1
    }


def save_evaluation_metrics(metrics, save_path):
    with open(save_path, 'w') as res_file:
        dw = csv.DictWriter(res_file,
                            fieldnames=[
                                'model_name', 'true_positives',
                                'true_negatives', 'false_positives',
                                'false_negatives', 'accuracy', 'sensitivity',
                                'specificity', 'precision', 'f1'
                            ])
        dw.writeheader()
        for i in metrics:
            dw.writerow(i)
