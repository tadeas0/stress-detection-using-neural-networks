from keras.callbacks import ModelCheckpoint, CSVLogger
import os

EPOCHS = 500


def train_model(model,
                x_train,
                y_train,
                save_path=None,
                log_path=None,
                checkpoint_dir=None):
    cb = []
    if not checkpoint_dir is None:
        cb.append(
            ModelCheckpoint(os.path.join(
                checkpoint_dir, 'model-{epoch:02d}-{val_accuracy:.2f}.hdf5'),
                            monitor='val_accuracy',
                            verbose=1,
                            save_best_only=False,
                            mode='max',
                            period=10))

    if not log_path is None:
        cb.append(CSVLogger(log_path, separator=',', append=True))

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    model.fit(x_train,
              y_train,
              epochs=EPOCHS,
              batch_size=32,
              validation_split=0.1,
              verbose=1,
              callbacks=cb)

    if not save_path is None:
        model.save(save_path)

    return model
