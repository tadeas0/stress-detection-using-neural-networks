import configparser
import argparse

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-w', '--wesad', type=str, required=False)
    ap.add_argument('-c', '--clas', type=str, required=False)
    args = ap.parse_args()

    cp = configparser.ConfigParser()
    cp.read('config.ini')

    if args.wesad:
        cp['DEFAULT']['wesad_dir'] = args.wesad

    if args.clas:
        cp['DEFAULT']['clas_dir'] = args.clas

    with open('config.ini', 'w') as config_file:
        cp.write(config_file)
