import multiprocessing
from real_time_analysis.consumer import consume
from real_time_analysis.producer import produce, ECGProducer
from real_time_analysis.visualizer import visualize

BUFFER_SIZE = 20000


def run_analysis_simulation(ecg, labels, model_path):
    ecg_producer = ECGProducer(ecg, labels)

    recorded_data = multiprocessing.Queue(BUFFER_SIZE)
    vis_queue = multiprocessing.Queue(BUFFER_SIZE)
    prediction_queue = multiprocessing.Queue(BUFFER_SIZE)

    producer = multiprocessing.Process(target=produce,
                                       args=(
                                           ecg_producer,
                                           recorded_data,
                                           vis_queue,
                                       ))

    consumer = multiprocessing.Process(target=consume,
                                       args=(
                                           recorded_data,
                                           prediction_queue,
                                           model_path,
                                       ))

    visualizer = multiprocessing.Process(target=visualize,
                                         args=(
                                             vis_queue,
                                             prediction_queue,
                                         ))

    consumer.start()
    visualizer.start()
    producer.start()
    visualizer.join()
    producer.terminate()
    consumer.terminate()
