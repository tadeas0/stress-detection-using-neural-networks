from keras.models import load_model
from collections import deque
import threading
from data_prep.ecg_preprocessing import filter_ecg_bw, get_hrv_prediction, get_hrv, scale_ecg
import config
import numpy as np
from real_time_analysis.constants import *


def consume(recorded_data, prediction_queue, model_path):
    model = load_model(model_path)
    consumer_buffer = deque(maxlen=MODEL_LEN)
    label_buffer = deque(maxlen=MODEL_LEN)
    t = None

    while True:
        if (t == None
                or not t.is_alive()) and len(consumer_buffer) == MODEL_LEN:
            t = threading.Thread(target=predict,
                                 args=(np.array(consumer_buffer), model,
                                       prediction_queue))
            t.daemon = True
            t.start()

        data = recorded_data.get()
        if data is None:
            break

        consumer_buffer.append(data[0])
        label_buffer.append(data[1])


def predict(data, model, prediction_queue):
    ecg_filter = filter_ecg_bw(data, config.TARGET_SAMPLE_RATE)
    ecg_prep = scale_ecg(ecg_filter)
    x = ecg_prep.reshape(1, -1, 1)
    prediction = np.round(model.predict(x).flatten())[0]
    hrv = get_hrv(ecg_filter, config.TARGET_SAMPLE_RATE)
    hrv_predict = get_hrv_prediction(hrv)
    result = {'prediction': prediction, **hrv, **hrv_predict}

    prediction_queue.put(result)