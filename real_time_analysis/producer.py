from real_time_analysis.constants import PRODUCER_SAMPLES
import time


class ECGProducer:
    def __init__(self, ecg_signal, labels=None):
        self.ecg_signal = ecg_signal
        self.current_index = 0
        self.labels = labels

    def get_data(self, amount):
        ci = self.current_index
        self.current_index += amount
        return self.ecg_signal[ci:ci + amount]

    def get_remaining_len(self):
        return len(self.ecg_signal) - self.current_index - 1

    def get_value(self):
        ci = self.current_index
        self.current_index += 1
        return self.ecg_signal[ci]

    def get_label(self):
        return self.labels[self.current_index]


def produce(ecg_producer, ecg_queue, vis_queue):
    i = 0
    while True:
        i += 1
        if ecg_producer.get_remaining_len() == 0:
            ecg_queue.put(None)
            if vis_queue:
                vis_queue.put(None)
            break

        value = ecg_producer.get_value()
        label = ecg_producer.get_label()

        ecg_queue.put((value, label))
        vis_queue.put((value, label))
        if i >= PRODUCER_SAMPLES:
            i = 0
            time.sleep(0.4)