from math import floor
import pygame as pg
from collections import deque
import numpy as np
import config
from real_time_analysis.constants import *


def visualize(vis_queue, prediction_queue):
    pg.init()
    pg.font.init()
    myfont = pg.font.Font(pg.font.get_default_font(), 20)
    screen = pg.display.set_mode(SCREEN_RES)

    ecg_buffer = deque(maxlen=MODEL_LEN)
    label_buffer = deque(maxlen=MODEL_LEN)
    prediction = {
        'heart_rate': -1,
        'mean_rr': -1,
        'rmssd': -1,
        'prediction': -1,
        'heart_rate_pred': -1,
        'mean_rr_pred': -1,
        'rmssd_pred': -1
    }
    running = True

    while running:
        data = vis_queue.get()

        if data is None:
            break

        ecg_buffer.append(data[0])
        label_buffer.append(data[1])

        if not prediction_queue.empty():
            prediction = prediction_queue.get()

        if len(ecg_buffer) > 2 and vis_queue.empty():
            screen.fill(BACKGROUND_COLOR)
            draw_ecg(np.array(ecg_buffer), screen)
            draw_text(prediction, label_buffer[-1], screen, myfont)
            draw_axes(screen)

            pg.display.flip()

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    running = False

    pg.quit()


def draw_text(prediction, label, screen, myfont):
    fp = format_prediction(prediction)
    pred_ts = myfont.render(f'Neural network prediction: {fp["prediction"]}',
                            True, TEXT_COLOR)

    real_ts = myfont.render(f'Real: {config.WESAD_LABEL_STRINGS[label]}', True,
                            TEXT_COLOR)

    hr_ts = myfont.render(f'Heart rate (bpm): {fp["heart_rate"]}', True,
                          TEXT_COLOR)

    mrr_ts = myfont.render(f'Mean RR (s): {fp["mean_rr"]}', True, TEXT_COLOR)

    srr_ts = myfont.render(f'RMSSD (s): {fp["rmssd"]}', True, TEXT_COLOR)

    hr_pred_ts = myfont.render(f'HR pred: {fp["heart_rate_pred"]}', True,
                               TEXT_COLOR)
    mrr_pred_ts = myfont.render(f'Mean RR pred: {fp["mean_rr_pred"]}', True,
                                TEXT_COLOR)
    srr_pred_ts = myfont.render(f'RMSSD pred: {fp["rmssd_pred"]}', True,
                                TEXT_COLOR)

    screen.blit(pred_ts, (10, 10))
    screen.blit(real_ts, (SCREEN_RES[0] - real_ts.get_width() - 20, 10))
    screen.blit(hr_ts, (10, SCREEN_RES[1] - hr_ts.get_height() -
                        srr_ts.get_height() - mrr_ts.get_height() - 10))
    screen.blit(
        mrr_ts,
        (10, SCREEN_RES[1] - mrr_ts.get_height() - srr_ts.get_height() - 10))
    screen.blit(srr_ts, (10, SCREEN_RES[1] - srr_ts.get_height() - 10))

    screen.blit(hr_pred_ts, (500, SCREEN_RES[1] - hr_ts.get_height() -
                             srr_ts.get_height() - mrr_ts.get_height() - 10))
    screen.blit(
        mrr_pred_ts,
        (500, SCREEN_RES[1] - mrr_ts.get_height() - srr_ts.get_height() - 10))
    screen.blit(srr_pred_ts, (500, SCREEN_RES[1] - srr_ts.get_height() - 10))


def format_prediction(prediction):
    round_or_string = lambda a, r: 'Waiting for more data' if a == -1 else round(
        a, r)
    hr = 'Waiting for more data' if prediction['heart_rate'] == -1 else int(
        prediction['heart_rate'])
    return {
        'prediction': prediction_to_text(prediction['prediction']),
        'heart_rate': hr,
        'mean_rr': round_or_string(prediction['mean_rr'], 3),
        'rmssd': round_or_string(prediction['rmssd'], 3),
        'heart_rate_pred':
        hrv_prediction_to_text(prediction['heart_rate_pred']),
        'mean_rr_pred': hrv_prediction_to_text(prediction['mean_rr_pred']),
        'rmssd_pred': hrv_prediction_to_text(prediction['rmssd_pred'])
    }


def prediction_to_text(prediction):
    text = {
        -1: "Waiting for more data",
        0: "Low cognitive load",
        1: "High cognitive load",
    }

    return text[prediction]


def hrv_prediction_to_text(prediction):
    text = {-1: "Uncertain", 0: "Low cognitive load", 1: "High cognitive load"}
    return text[prediction]


def draw_ecg(ecg, screen):
    ecg_len = len(ecg)
    segment_width = SCREEN_RES[0] / MODEL_LEN
    points = [(i * segment_width, (ecg[i] * -ECG_VIS_MULTI) + ECG_BASELINE_Y)
              for i in range(ecg_len)]

    pg.draw.aalines(screen, ECG_COLOR, False, points)


def draw_axes(screen):
    tikz_num = MODEL_LEN / config.TARGET_SAMPLE_RATE
    tikz_dist = SCREEN_RES[0] / tikz_num
    for i in range(floor(tikz_num)):
        y1 = ECG_BASELINE_Y - floor(TIKZ_SIZE / 2)
        y2 = ECG_BASELINE_Y + floor(TIKZ_SIZE / 2)
        x = i * tikz_dist
        pg.draw.aaline(screen, AXIS_COLOR, (x, y1), (x, y2))
    pg.draw.aaline(screen, AXIS_COLOR, (0, ECG_BASELINE_Y),
                   (SCREEN_RES[0], ECG_BASELINE_Y))

    for i in range(-2, 3):
        y = ECG_BASELINE_Y + i * ECG_VIS_MULTI
        pg.draw.aaline(screen, AXIS_COLOR, (0, y), (TIKZ_SIZE, y))