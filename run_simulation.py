import logging
from util.util import setup_logging
from real_time_analysis.analysis_simulation import run_analysis_simulation
import pandas as pd
import os
import config
import argparse

if __name__ == '__main__':
    setup_logging()
    ap = argparse.ArgumentParser()
    ap.add_argument('data_path', type=str)
    args = ap.parse_args()
    data_path = args.data_path
    model_path = os.path.join(config.EXAMPLE_DATA_DIR,
                              'model3_preprocessed_30s.hdf5')

    logging.info('Loading data')
    try:
        df = pd.read_pickle(data_path)
        ecg = df['ECG'].to_numpy()
        labels = df['label'].to_numpy()
        run_analysis_simulation(ecg, labels, model_path)
        logging.info('Simulation finished')
    except FileNotFoundError as e:
        logging.error('Invalid data file')
        logging.error(e)
    except Exception as e:
        logging.error('Something went wrong')
        logging.error(e)
