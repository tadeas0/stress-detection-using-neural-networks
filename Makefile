PYTHON=python3
TRAIN=${PYTHON} run_experiment.py
SET_PATHS=${PYTHON} set_paths.py
EVALUATE=${PYTHON} run_evaluation.py

.DEFAULT_GOAL = help

help:
	@echo "---------------------------------- HELP --------------------------------------"
	@echo "make realtime - run real time analysis simulation on sample data"
	@echo "make realtime2 - run real time analysis simulation on another sample data"
	@echo "make qrs - run QRS detection"
	@echo "TRAINING:"
	@echo "make train1 - train neural networks with 1s window"
	@echo "make train5 - train neural networks with 5s window"
	@echo "make train10 - train neural networks with 10s window"
	@echo "make train20 - train neural networks with 20s window"
	@echo "make train30 - train neural networks with 30s window"
	@echo "make train30un - train neural networks with 30s window and unprocessed signal"
	@echo "make eval - run evaluation for pretrained model"
	@echo "make eval MODEL=<path> WINDOW_LEN=<len> PREPROCESS=<TRUE/FALSE> - run evaluation for model saved in <path>, trained with window length <len> on preprocessed/unprocessed signal"
	@echo "make path - set dataset directories Usage: 'make path WESAD=<dir> CLAS=<dir>'"

qrs:
	${PYTHON} run_qrs_detection.py

realtime:
	${PYTHON} run_simulation.py ./example_data/ecg_showcase.pkl

realtime2:
	${PYTHON} run_simulation.py ./example_data/ecg_showcase_2.pkl

train1:
	${TRAIN} 1 -p

train5:
	${TRAIN} 5 -p

train10:
	${TRAIN} 10 -p

train20:
	${TRAIN} 20 -p

train30:
	${TRAIN} 30 -p

train30un:
	${TRAIN} 30

path:
	@if [ ! -z ${CLAS} ]; then \
		${SET_PATHS} -c ${CLAS}; \
	fi; \
	if [ ! -z ${WESAD} ]; then \
		${SET_PATHS} -w ${WESAD}; \
	fi;

eval:
	@if [ -z ${MODEL} ] && [ -z ${WINDOW_LEN} ] && [ -z ${PREPROCESS} ]; then \
		${EVALUATE}; \
	elif [ -z ${MODEL} ] || [ -z ${WINDOW_LEN} ] || [ -z ${PREPROCESS} ]; then \
		echo "Please specify MODEL, WINDOW_LEN and PREPROCESS"; \
	elif [ ${PREPROCESS} = "TRUE" ]; then \
		${EVALUATE} -p -m ${MODEL} -w ${WINDOW_LEN}; \
	elif [ ${PREPROCESS} = "FALSE" ]; then \
		${EVALUATE} -m ${MODEL} -w ${WINDOW_LEN}; \
	else \
		echo "Incorrect PREPROCESS value"; \
	fi;
