import logging
from util.util import setup_logging, create_dirs, remove_dirs
from data_prep.full_data_prep import create_test_train_datasets
from data_prep.wesad_data_prep import preprocess_wesad
from data_prep.clas_data_prep import preprocess_clas
from keras.models import load_model
from neural_networks.evaluation import get_evaluation_metrics, save_evaluation_metrics
import config
import os
import argparse


def parse_arguments():
    ap = argparse.ArgumentParser()
    default_m_path = os.path.join(config.EXAMPLE_DATA_DIR,
                                  'model3_preprocessed_30s.hdf5')
    ap.add_argument('-m', '--model', type=str, default=default_m_path)
    ap.add_argument('-w',
                    '--window',
                    type=int,
                    default=30,
                    choices=[1, 5, 10, 20, 30])
    ap.add_argument('-p', '--preprocess', action='store_true')
    args = ap.parse_args()
    return args.model, args.window, args.preprocess


if __name__ == '__main__':
    setup_logging()
    try:
        parse_arguments()
        model_path, window, preprocess = parse_arguments()
        create_dirs([
            config.OUT_DIR, config.CLAS_FORMATTED_DIR,
            config.WESAD_FORMATTED_DIR
        ])

        preprocess_clas(config.CLAS_ORIGINAL_DIR, config.CLAS_FORMATTED_DIR,
                        window, preprocess)
        preprocess_wesad(config.WESAD_ORIGINAL_DIR, config.WESAD_FORMATTED_DIR,
                         window, preprocess)
        x_train, y_train, x_test, y_test = create_test_train_datasets(
            [config.CLAS_FORMATTED_DIR, config.WESAD_FORMATTED_DIR])

        model = load_model(model_path)
        em = get_evaluation_metrics(model, x_test, y_test)
        em['model_name'] = 'model'

        save_evaluation_metrics([em],
                                os.path.join(config.OUT_DIR, 'results.csv'))
    except Exception as e:
        logging.error(e)
    finally:
        remove_dirs([config.CLAS_FORMATTED_DIR, config.WESAD_FORMATTED_DIR])