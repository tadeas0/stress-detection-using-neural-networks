import os
import config
import logging
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import shutil


def get_x_y_sh(df, input_parameters):
    x = df[input_parameters].to_numpy()
    x = np.apply_along_axis(np.stack, 0, x)
    y = df['model_label']

    return x, y


def get_x_y_mh(df, input_parameters):
    x = []
    for p in input_parameters:
        tmp = df[p].to_numpy()
        tmp = np.stack(tmp)
        tmp = np.apply_along_axis(np.expand_dims, 1, tmp, 1)

        x.append(tmp)

    y = df['model_label']

    return x, y


def create_sliding_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window, 1)
    strides = (a.strides[0], a.strides[0], a.strides[0])
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


def setup_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


def create_dirs(dirs):
    for d in dirs:
        if not os.path.exists(d):
            os.makedirs(d)


def remove_dirs(dirs):
    for d in dirs:
        if os.path.exists(d):
            shutil.rmtree(d, ignore_errors=True)


def balance_classes(df):
    df_c0 = df[df['model_label'] == 0]
    df_c1 = df[df['model_label'] == 1]
    c0_count = df_c0.shape[0]
    c1_count = df_c1.shape[0]
    if c0_count > c1_count:
        df = pd.concat(
            [df_c0.sample(c1_count, random_state=config.RANDOM_STATE), df_c1])
    elif c0_count < c1_count:
        df = pd.concat(
            [df_c0,
             df_c1.sample(c0_count, random_state=config.RANDOM_STATE)])
    df = df.sample(frac=1, random_state=config.RANDOM_STATE)
    return df


def downsample_labels(labels, old_sample_rate, new_sample_rate):
    factor = old_sample_rate / new_sample_rate
    n = np.ceil(labels.size / factor)
    f = interp1d(np.linspace(0, 1, labels.size), labels, kind='nearest')
    return f(np.linspace(0, 1, n))