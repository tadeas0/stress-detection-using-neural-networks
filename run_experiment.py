from neural_networks.evaluation import get_evaluation_metrics, save_evaluation_metrics
from neural_networks.training import train_model
from data_prep.wesad_data_prep import preprocess_wesad
import config
from data_prep.clas_data_prep import preprocess_clas
import logging
from util.util import create_dirs, remove_dirs, setup_logging
from data_prep.full_data_prep import create_test_train_datasets
from neural_networks.models import singleheaded
import os
import argparse


def parse_arguments():
    ap = argparse.ArgumentParser()
    ap.add_argument('window_len', type=int, choices=[1, 5, 10, 20, 30])
    ap.add_argument('-p', '--preprocess', action='store_true')
    args = ap.parse_args()
    return args.window_len, args.preprocess


if __name__ == '__main__':
    setup_logging()
    try:
        window_len, preprocess = parse_arguments()
        prep_string = 'preprocessed' if preprocess else 'unprocessed'

        od = os.path.join(config.TRAIN_OUT_DIR, f'{prep_string}_{window_len}s')
        models_dir = os.path.join(od, 'saved_models')
        training_log_dir = os.path.join(od, 'training_logs')

        create_dirs([
            models_dir, training_log_dir, config.CLAS_FORMATTED_DIR,
            config.WESAD_FORMATTED_DIR
        ])

        preprocess_clas(config.CLAS_ORIGINAL_DIR, config.CLAS_FORMATTED_DIR,
                        window_len, preprocess)
        preprocess_wesad(config.WESAD_ORIGINAL_DIR, config.WESAD_FORMATTED_DIR,
                         window_len, preprocess)

        x_train, y_train, x_test, y_test = create_test_train_datasets(
            split_dasatet_dirs=[
                config.CLAS_FORMATTED_DIR, config.WESAD_FORMATTED_DIR
            ])

        input_shapes = [x_train.shape[1], x_train.shape[2]]

        models = {
            'model1': singleheaded.sh_model1(input_shapes),
            'model2': singleheaded.sh_model2(input_shapes),
        }

        if window_len >= 20:
            models['model3'] = singleheaded.sh_model3(input_shapes)

        metrics = []
        logging.info('Starting training')
        for k, v in models.items():
            logging.info(f'Training model {k}')
            model_save_path = os.path.join(models_dir, f'{k}.hdf5')
            training_log_path = os.path.join(training_log_dir, f'{k}.csv')
            model = train_model(v,
                                x_train,
                                y_train,
                                save_path=model_save_path,
                                log_path=training_log_path)

            logging.info(f'Evaluating model {k}')
            m = get_evaluation_metrics(model, x_test, y_test)
            m['model_name'] = k
            metrics.append(m)

        save_evaluation_metrics(metrics, os.path.join(od, 'results.csv'))
    except Exception as e:
        logging.error(e)
    finally:
        remove_dirs([config.CLAS_FORMATTED_DIR, config.WESAD_FORMATTED_DIR])