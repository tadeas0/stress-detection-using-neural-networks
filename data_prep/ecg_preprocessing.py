import neurokit2 as nk
import numpy as np
from scipy.signal import resample, medfilt
import config


def filter_ecg_bw(ecg_signal, sampling_rate):
    ecg_filter = nk.signal_filter(ecg_signal,
                                  lowcut=1,
                                  sampling_rate=sampling_rate,
                                  order=5)

    return ecg_filter


def filter_ecg_median(ecg_signal, sampling_rate):
    kernel_size = (sampling_rate * 3) + (((sampling_rate * 3) % 2) + 1)
    return medfilt(ecg_signal, kernel_size)


def filter_ecg_powerline(ecg_signal, sampling_rate):
    return nk.signal_filter(ecg_signal,
                            sampling_rate,
                            method='powerline',
                            powerline=50,
                            order=5)


def scale_ecg(ecg_signal):
    return (ecg_signal - ecg_signal.mean()) / (ecg_signal.std())


def preprocess_ecg(ecg_signal, sampling_rate):
    return scale_ecg(filter_ecg_bw(ecg_signal, sampling_rate))


def filter_ecg_qrs(ecg_signal, sampling_rate):
    return filter_ecg_powerline(filter_ecg_bw(ecg_signal, sampling_rate),
                                sampling_rate)


def create_windows(ecg_signal, sample_rate, window_len):
    """Creates windows on ecg signal. Removes first elements, that do not fit into window

    Parameters
    ----------
    ecg_signal : np.array
        ECG signal
    sample_rate : int
        Sampling frequency of `ecg_signal`
    window_len : int
        Length of windows in seconds
    """
    split_num = int(len(ecg_signal) / (sample_rate * window_len))
    short = ecg_signal[len(ecg_signal) % int(sample_rate * window_len):]
    return np.split(short, split_num)


def resample_signal(signal, old_sample_rate, new_sample_rate):
    new_num_samples = int(len(signal) / old_sample_rate) * new_sample_rate
    return resample(signal, new_num_samples)


def get_hrv(ecg_signal, sample_rate):
    _, peaks = nk.ecg_peaks(ecg_signal, sample_rate)
    r_peaks = peaks['ECG_R_Peaks']

    hrv = nk.hrv_time(peaks, sample_rate)

    duration_sec = len(ecg_signal) / sample_rate

    heart_rate = (60 / duration_sec) * len(r_peaks)
    mean_rr = hrv['HRV_MeanNN'].iloc[0] / 1000
    rmssd = hrv['HRV_RMSSD'].iloc[0] / 1000

    return {'heart_rate': heart_rate, 'mean_rr': mean_rr, 'rmssd': rmssd}


def get_hrv_prediction(hrv_dict):
    res_dict = {
        'heart_rate_pred': -1,
        'mean_rr_pred': -1,
        'rmssd_pred': -1,
    }
    if hrv_dict['heart_rate'] >= config.HRV_BOUNDS['heart_rate_cog_load']:
        res_dict['heart_rate_pred'] = 1
    if hrv_dict['heart_rate'] <= config.HRV_BOUNDS['heart_rate_calm']:
        res_dict['heart_rate_pred'] = 0
    if hrv_dict['mean_rr'] >= config.HRV_BOUNDS['mean_rr_calm']:
        res_dict['mean_rr_pred'] = 0
    if hrv_dict['mean_rr'] <= config.HRV_BOUNDS['mean_rr_cog_load']:
        res_dict['mean_rr_pred'] = 1
    if hrv_dict['rmssd'] <= config.HRV_BOUNDS['rmssd_cog_load']:
        res_dict['rmssd_pred'] = 1
    if hrv_dict['rmssd'] >= config.HRV_BOUNDS['rmssd_calm']:
        res_dict['rmssd_pred'] = 0
    return res_dict


def detect_qrs(ecg_signal, sampling_rate):
    _, r_peak_dict = nk.ecg_peaks(ecg_signal, sampling_rate=sampling_rate)
    r_peak_arr = r_peak_dict['ECG_R_Peaks']
    _, peak_dict = nk.ecg_delineate(ecg_signal, r_peak_arr, sampling_rate)
    q_peak_arr = peak_dict['ECG_Q_Peaks']
    s_peak_arr = peak_dict['ECG_S_Peaks']

    return {
        'q_peaks': q_peak_arr,
        'r_peaks': r_peak_arr,
        's_peaks': s_peak_arr
    }