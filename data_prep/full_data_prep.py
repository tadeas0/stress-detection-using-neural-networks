import logging
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
import os
import config
from util.util import get_x_y_sh, balance_classes


def load_directory(path):
    df = pd.DataFrame()
    for i in os.listdir(path):
        df = df.append(pd.read_pickle(os.path.join(path, i)))
    return df.loc[(df['model_label'] == 1) | (df['model_label'] == 0)]


def create_test_train_datasets(split_dasatet_dirs=[],
                               train_dataset_dirs=[],
                               test_dataset_dirs=[],
                               should_balance_classes=True,
                               train_output_path=None,
                               test_output_path=None):
    logging.info('Creating testing and training dataset')
    df_split = pd.DataFrame()
    for ds in split_dasatet_dirs:
        logging.info(f'Split df: Loading {ds}')
        df_split = df_split.append(load_directory(ds))

    df_train, df_test = train_test_split(df_split,
                                         test_size=0.3,
                                         random_state=config.RANDOM_STATE)

    for ds in train_dataset_dirs:
        logging.info(f'Train df: Loading {ds}')
        df_train = df_train.append(load_directory(ds), ignore_index=True)

    for ds in test_dataset_dirs:
        logging.info(f'Test df: Loading {ds}')
        df_test = df_test.append(load_directory(ds), ignore_index=True)

    df_train = df_train.sample(frac=1, random_state=config.RANDOM_STATE)
    df_test = df_test.sample(frac=1, random_state=config.RANDOM_STATE)

    if should_balance_classes:
        logging.info(f'Balancing classes in training dataset')
        df_train = balance_classes(df_train)

    if not train_output_path is None:
        logging.info(f'Saving training df to {train_output_path}')
        df_train.to_pickle(train_output_path)
    if not test_output_path is None:
        logging.info(f'Saving testing df to {train_output_path}')
        df_test.to_pickle(test_output_path)

    x_train, y_train = get_x_y_sh(df_train, ['ECG'])
    x_test, y_test = get_x_y_sh(df_test, ['ECG'])

    return x_train, y_train, x_test, y_test
