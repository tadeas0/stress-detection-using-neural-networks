import logging
import os
import re
import pandas as pd
import config
from util.util import setup_logging
from data_prep.ecg_preprocessing import preprocess_ecg, create_windows, resample_signal


def preprocess_clas(orig_dir, dst_dir, window_len, preprocess_signal=True):
    logging.info('Preprocessing CLAS dataset')
    logging.info(f'Window length: {window_len}s')
    bd_dir = os.path.join(orig_dir, 'Block_details')
    part_dir = os.path.join(orig_dir, 'Participants')

    bd_parts = os.listdir(bd_dir)
    bd_parts = sorted(bd_parts, key=lambda a: int(a.split('_')[0][4:]))

    for i, b in enumerate(bd_parts):
        part_df = pd.DataFrame()
        part = b.split('_')[0]
        block_df = pd.read_csv(os.path.join(bd_dir, b))

        dst = os.path.join(dst_dir, f'{i}.pkl')
        file_list = os.listdir(os.path.join(part_dir, part, 'by_block'))

        logging.info(f'{os.path.join(part_dir, part)} -> {dst}')

        part_df = pd.DataFrame()
        for _, row in block_df.iterrows():
            block = row['Block']
            r = re.compile(f'^{block}_ecg_')
            file_name = list(filter(r.match, file_list))[0]

            label = config.CLAS_LABEL_STRINGS[row['Block Type']]
            ecg_raw = pd.read_csv(
                os.path.join(part_dir, part, 'by_block',
                             file_name))['ecg2'].to_numpy()

            ecg_upsample = resample_signal(ecg_raw, config.CLAS_SAMPLING_RATE,
                                           config.TARGET_SAMPLE_RATE)
            if len(ecg_upsample) > 18:
                ecg_prep = ecg_upsample
                if preprocess_signal:
                    ecg_prep = preprocess_ecg(ecg_upsample,
                                              config.TARGET_SAMPLE_RATE)
                if len(ecg_prep) > window_len * config.TARGET_SAMPLE_RATE:
                    ecg_win = create_windows(ecg_prep,
                                             config.TARGET_SAMPLE_RATE,
                                             window_len)

                    model_label = -1
                    if label in config.CLAS_POSITIVE:
                        model_label = 1
                    elif label in config.CLAS_NEGATIVE:
                        model_label = 0

                    df_tmp = pd.DataFrame({
                        'ECG': ecg_win,
                        'label': label,
                        'model_label': model_label
                    })
                    part_df = part_df.append(df_tmp, ignore_index=True)

        part_df.to_pickle(dst)


if __name__ == '__main__':
    setup_logging()
    if not os.path.exists(config.CLAS_FORMATTED_DIR):
        os.makedirs(config.CLAS_FORMATTED_DIR)
    preprocess_clas(config.CLAS_ORIGINAL_DIR, config.CLAS_FORMATTED_DIR)
