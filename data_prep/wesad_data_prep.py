from data_prep.ecg_preprocessing import preprocess_ecg, create_windows, resample_signal
import logging
import os
from util.util import setup_logging
from scipy.interpolate import interp1d
import config
import pickle
import pandas as pd
import numpy as np


def load_wesad_dict(path):
    f = open(path, 'rb')
    p = pickle.load(f, encoding='latin1')
    f.close()
    return p


def format_wesad_dict(wesad_dict):
    chest_sensor = wesad_dict['signal']['chest']

    df = pd.DataFrame({
        'ECG': chest_sensor['ECG'].flatten(),
        'label': wesad_dict['label']
    })

    first = df[df['label'] == config.WesadLabel.STRESS.value].index[0]
    last = df[df['label'] == config.WesadLabel.STRESS.value].index[-1]
    df.loc[first + int(5.5 * 60 * config.RESPIBAN_SAMPLE_RATE):last,
           'label'] = config.WesadLabel.COG_LOAD.value

    return df


def preprocess_wesad_dict(wesad_dict, window_len, preprocess_signal=True):
    df_orig = format_wesad_dict(wesad_dict)
    ecg_orig = df_orig['ECG'].to_numpy()
    ecg_down = resample_signal(ecg_orig, config.RESPIBAN_SAMPLE_RATE,
                               config.TARGET_SAMPLE_RATE)

    ecg_prep = ecg_down
    if preprocess_signal:
        ecg_prep = preprocess_ecg(ecg_down, config.TARGET_SAMPLE_RATE)

    labels = df_orig['label'].to_numpy()
    xp = np.arange(0, len(labels),
                   config.RESPIBAN_SAMPLE_RATE / config.TARGET_SAMPLE_RATE)
    labels_down = interp1d(np.arange(len(labels)), labels, kind='nearest')(xp)

    df_prep = pd.DataFrame({'ECG': ecg_prep, 'label': labels_down})

    df_prep['group'] = df_prep['label'].ne(df_prep['label'].shift()).cumsum()
    df_grouped = df_prep.groupby('group')

    df_win = pd.DataFrame()
    for _, g in df_grouped:
        if len(g) > config.TARGET_SAMPLE_RATE * window_len:
            ecg_win = create_windows(g['ECG'].to_numpy(),
                                     config.TARGET_SAMPLE_RATE, window_len)
            label = g['label'].iloc[0]

            model_label = -1
            if label in config.WESAD_POSITIVE:
                model_label = 1
            elif label in config.WESAD_NEGATIVE:
                model_label = 0

            df_tmp = pd.DataFrame({
                'ECG': ecg_win,
                'label': label,
                'model_label': model_label
            })
            df_win = df_win.append(df_tmp, ignore_index=True)

    return df_win


def preprocess_wesad(orig_dir, dst_dir, window_len, preprocess_signal=True):
    logging.info(f'Preprocessing WESAD dataset {orig_dir} -> {dst_dir}')
    logging.info(f'Window length: {window_len}s')
    parts = [
        i for i in os.listdir(orig_dir)
        if os.path.isdir(os.path.join(orig_dir, i))
    ]

    parts = sorted(parts, key=lambda a: int(a[1:]))

    for i, p in enumerate(parts):
        src = os.path.join(orig_dir, p, f'{p}.pkl')
        dst = os.path.join(dst_dir, f'{i}.pkl')

        logging.info(f'{src} -> {dst}')

        wesad_dict = load_wesad_dict(src)
        df = preprocess_wesad_dict(wesad_dict, window_len, preprocess_signal)
        df.to_pickle(dst)


if __name__ == '__main__':
    setup_logging()
    if not os.path.exists(config.WESAD_FORMATTED_DIR):
        os.makedirs(config.WESAD_FORMATTED_DIR)
    preprocess_wesad(config.WESAD_ORIGINAL_DIR, config.WESAD_FORMATTED_DIR)
