import logging
from util.util import setup_logging
from data_prep.ecg_preprocessing import detect_qrs, filter_ecg_bw, filter_ecg_powerline
import os
import matplotlib.pyplot as plt
import pandas as pd
import config


def plot_peaks(ecg_signal, peaks, ax):
    qy = [ecg_signal[i] for i in peaks['q_peaks']]
    ry = [ecg_signal[i] for i in peaks['r_peaks']]
    sy = [ecg_signal[i] for i in peaks['s_peaks']]
    ax.plot(ecg_signal, label='ECG signal')
    ax.scatter(peaks['q_peaks'], qy, color='y', label='Q-Peaks')
    ax.scatter(peaks['r_peaks'], ry, color='r', label='R-Peaks')
    ax.scatter(peaks['s_peaks'], sy, color='g', label='S-Peaks')


if __name__ == '__main__':
    setup_logging()
    logging.info('Loading data')
    path = os.path.join(config.EXAMPLE_DATA_DIR, 'qrs_detection.pkl')
    ecg_raw = pd.read_pickle(path)['ECG'].to_numpy()

    logging.info('Filtering signal')
    ecg_bw = filter_ecg_bw(ecg_raw, config.RESPIBAN_SAMPLE_RATE)
    ecg_pl = filter_ecg_powerline(ecg_bw, config.RESPIBAN_SAMPLE_RATE)

    logging.info('Detecting QRS data')
    qrs_raw = detect_qrs(ecg_raw, config.RESPIBAN_SAMPLE_RATE)
    qrs_bw = detect_qrs(ecg_bw, config.RESPIBAN_SAMPLE_RATE)
    qrs_pl = detect_qrs(ecg_pl, config.RESPIBAN_SAMPLE_RATE)

    fig, ax = plt.subplots(3, sharex=True, sharey=True)
    ax[0].set_title('Raw signal')
    ax[1].set_title('Butterworth filter')
    ax[2].set_title('Butterworth filter, moving average filter')
    plot_peaks(ecg_raw, qrs_raw, ax[0])
    plot_peaks(ecg_bw, qrs_bw, ax[1])
    plot_peaks(ecg_pl, qrs_pl, ax[2])
    handles, labels = ax[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right')
    plt.show()
